CREATE DATABASE IF NOT EXISTS job;
USE job;
CREATE TABLE IF NOT EXISTS workers (
first_name VARCHAR(20) NOT NULL,
last_name VARCHAR(20) NOT NULL,
post VARCHAR(30) NOT NULL,
salary INT NOT NULL
);
INSERT INTO workers (first_name, last_name, post, salary) VALUES ('Igor', 'Ivanov', 'Administrator', 20000);
INSERT INTO workers (first_name, last_name, post, salary) VALUES ('Dmitry', 'Frolov', 'designer', 25000);
INSERT INTO workers (first_name, last_name, post, salary) VALUES ('Tatyana', 'Ivanov', 'Administrator', 34000);
ALTER TABLE workers ADD COLUMN id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY  FIRST;
INSERT INTO workers (first_name, last_name, post, salary) VALUES ('Denis', 'Korobkov', 'Desinger', 20000);
UPDATE workers SET post = 'Designer' WHERE id IN (2,4);
INSERT INTO workers(first_name, last_name, post, salary) VALUES ( 'Sergei', 'Isaev', 'Boss', 50000 );
UPDATE workers SET salary = 31000 WHERE id = 4;
SELECT first_name, last_name  FROM workers WHERE salary < 30000;
SELECT first_name, last_name  FROM workers WHERE salary < 30000 AND post = 'Designer';
CREATE TABLE IF NOT EXISTS bosses (
id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
first_name VARCHAR(20) NOT NULL,
last_name VARCHAR(20) NOT NULL
);
INSERT INTO bosses (id, first_name, last_name) VALUES (100, 'Sergei', 'Petrov');
INSERT INTO bosses (first_name, last_name) VALUES ('Dmitry', 'Ivanov');
INSERT INTO bosses (first_name, last_name) VALUES ('Alexander', 'Smirnov');
CREATE TABLE IF NOT EXISTS subordinate (
id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
first_name VARCHAR(20) NOT NULL,
last_name VARCHAR(20) NOT NULL
);
INSERT INTO subordinate (id, first_name, last_name) VALUES (200, 'Elena', 'Petrova');
INSERT INTO subordinate (first_name, last_name) VALUES ('Artem', 'Chernov');
INSERT INTO subordinate (first_name, last_name) VALUES ('Vladimir', 'Krasnov');
INSERT INTO subordinate (first_name, last_name) VALUES ('Daniil', 'Komarov');
INSERT INTO subordinate (first_name, last_name) VALUES ('Alexander', 'Petrov');
CREATE TABLE IF NOT EXISTS bosses_workers (
bosses_id INT UNSIGNED NOT NULL,
workers_id INT UNSIGNED NOT NULL,
PRIMARY KEY(bosses_id, workers_id),
FOREIGN KEY(bosses_id) REFERENCES bosses(id),
FOREIGN KEY(workers_id) REFERENCES workers(id)
);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (100, 1);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (100, 3);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (101, 2);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (101, 4);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (102, 1);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (102, 2);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (102, 3);
INSERT INTO bosses_workers (bosses_id, workers_id) VALUES (102, 4);
CREATE TABLE IF NOT EXISTS workers_subordinate (
workers_id INT UNSIGNED NOT NULL,
subordinate_id INT UNSIGNED NOT NULL,
PRIMARY KEY(workers_id, subordinate_id),
FOREIGN KEY(workers_id) REFERENCES workers(id),
FOREIGN KEY(subordinate_id) REFERENCES subordinate(id)
);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (1, 203);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (2, 202);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (2, 201);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (4, 200);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (4, 204);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (5, 200);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (5, 201);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (5, 202);
INSERT INTO workers_subordinate(workers_id, subordinate_id) VALUES (5, 203);
SELECT w.first_name AS СОТРУДНИК, w.last_name AS И, s.first_name AS ЕГО, s.last_name AS ПОДЧИНЕННЫЕ 
FROM workers w INNER JOIN workers_subordinate w_s ON w.id = w_s.workers_id
INNER JOIN subordinate s ON s.id = w_s.subordinate_id WHERE w.id = 5;





